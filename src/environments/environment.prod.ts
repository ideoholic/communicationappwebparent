export const environment = {
  production: true
};

export const Globals = { 
  APP_URL: 'http://<APP_URL>/rest/',
  DATABASE_NAME: "indexed-db",
  DATABASE_VERSION: 1,
  COLLECTION_NAME: "products"
}

