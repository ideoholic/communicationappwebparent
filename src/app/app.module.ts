import { LayoutModule } from '@angular/cdk/layout';
import { OverlayModule } from '@angular/cdk/overlay';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MatPasswordStrengthModule } from '@angular-material-extensions/password-strength';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModuleLoader } from './material-module';
import { AuthService } from './shared/guard/auth.service';
import { TokenInterceptorService } from './token-interceptor.service';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { NavComponent } from './layout/nav/nav.component';
import { UsersComponent } from './layout/users/users.component';
import { NewuserComponent } from './layout/newuser/newuser.component';
import { ViewuserComponent } from './layout/viewuser/viewuser.component';
import { GroupsComponent } from './layout/groups/groups.component';
import { MatDialogModule } from '@angular/material';
import { MatCardModule, MatButtonModule} from '@angular/material';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

// AoT requires an exported function for factories
export const createTranslateLoader = (http: HttpClient) => {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};




@NgModule({
    declarations: [AppComponent,
		   NavComponent,
		   UsersComponent,
		   NewuserComponent,
		   ViewuserComponent,
		   GroupsComponent ],
    imports: [
        MatButtonModule,
        MatCardModule,
        MatDialogModule,
        MaterialModuleLoader,
        MatCheckboxModule,
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        FormsModule,
        LayoutModule,
        OverlayModule,
        MatDatepickerModule,
        MatNativeDateModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        MatPasswordStrengthModule,
        ServiceWorkerModule.register('./ngsw-worker.js', { enabled: environment.production })
         
    ],
   // entryComponents: [DialogOverviewExampleDialog,DashboardComponent],
    providers: [AuthService,
     {
       provide: HTTP_INTERCEPTORS,
       useClass: TokenInterceptorService,
       multi: true
     }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
