import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Router } from '@angular/router'

@Injectable()
export class AuthService {

  constructor(private http: HttpClient,
              private _router: Router) { }           

  logoutUser() {
    localStorage.removeItem('token');
    
    this._router.navigate(['/']);
  }

  setToken(tokenValue) {

    if(tokenValue.includes('Bearer'))
      tokenValue= tokenValue.replace('Bearer ','');
   
      localStorage.setItem('token', tokenValue);
  }

  getToken() {
    
    return localStorage.getItem('token');
  }

  loggedIn() {
    return !!localStorage.getItem('token');
  }
}
