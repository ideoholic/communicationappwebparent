import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Globals } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class GroupRestServices{
  constructor(private http:HttpClient){}

  getGroups(){
    let requestURL = Globals.APP_URL+"/group";
    return this.http.get<any>(requestURL);
  }

 

}