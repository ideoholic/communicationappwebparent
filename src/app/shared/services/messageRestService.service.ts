import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Globals } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MessageRestService {
  
  userId = '1';
  
  constructor(private http: HttpClient) { }

   getAllMessages(userId){
    let requestURL = Globals.APP_URL + userId + '/needs';
    return this.http.get<any>(requestURL);
 }

  deleteAMessage(messageId){
    let requestURL = Globals.APP_URL +this.userId + '/needs/'+messageId;
    return this.http.delete<any>(requestURL);
  }

    getASingleMessage(messageID){
    
    let loginUrl = Globals.APP_URL + this.userId+'/needs/'+messageID;
    return this.http.get<any>(loginUrl);
    }

    setMessage(Message){
      
      let requestURL;

      if(Message.users.length>0){
      requestURL = Globals.APP_URL+ this.userId+'/needs/messageId';
      return this.http.post<any>(requestURL,Message, {observe: 'response'});
     
    }else{
      requestURL = Globals.APP_URL+ this.userId+'/needs';
      return this.http.post<any>(requestURL,Message, {observe: 'response'});
    }
  }

  getParentMessage(id){
    console.log("Id "+id);
    let requestURL = Globals.APP_URL+ "1"+'/needs/parent/'+id;
    return this.http.get<any>(requestURL);
  }

}
