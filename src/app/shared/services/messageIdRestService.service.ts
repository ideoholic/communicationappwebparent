import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Globals } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MessageIdRestService {
   
   constructor(private http: HttpClient) { }

   deleteAMessage(messageId){
    var userId ='1';
    let requestURL = Globals.APP_URL + userId + '/needs/'+messageId;
    return this.http.delete<any>(requestURL);
 }

 
}
