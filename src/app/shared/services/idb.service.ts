import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';
import { ResolveEnd } from '../../../../node_modules/@angular/router';
import { resolve } from 'url';
import { reject } from '../../../../node_modules/@types/q';
import { SharingService } from './sharing.service';
import { Globals } from '../../../environments/environment';

declare const window: any;



@Injectable({
  providedIn: 'root'
})
export class IdbService {

  constructor() { }

  indexedDataBase(resolve, reject) {

    //if(getDBRef)
    const request = window.indexedDB.open(Globals.DATABASE_NAME, Globals.DATABASE_VERSION);

    request.onsuccess = function (evt) {
      // Equal to: db = req.result;
      const database = evt.target.result;

      resolve(database);
    };

    request.onerror = function (event) {
      console.error('openDb:', event.target.errorCode);
      reject();
    };

    request.onupgradeneeded = function (event) {
      console.log('openDb.onupgradeneeded' + event.currentTarget.result);

      this.objStore = event.currentTarget.result.createObjectStore(Globals.COLLECTION_NAME, { keyPath: 'id' });

      /*var store = event.currentTarget.result.createObjectStore(
  'DB_STORE_NAME', { keyPath: 'id', autoIncrement: true });*/
    };

  }


  storeData(database, products) {

    console.log('store Data ' + database);
    const transaction = database.transaction(Globals.COLLECTION_NAME, 'readwrite');

    transaction.onsuccess = function (event) {
      console.log('[Transaction] ALL DONE!');
    };

    transaction.onerror = function (event) {
      console.log('[Transaction] NOT DONE!' + transaction);
    };

    // get store from transaction
    // returns IDBObjectStore instance
    const productsStore = transaction.objectStore(Globals.COLLECTION_NAME);
    // put products data in productsStore
    products.forEach(function (product) {
      let db_op_req = productsStore.add(product);
      console.log('the data ' + db_op_req);
      // IDBRequest
    });

  }


  getStoredData(dbd, resolve) {

    console.log('Inside stored Indexed DB :' + dbd);
    const transaction = dbd.transaction(Globals.COLLECTION_NAME, 'readwrite');

    transaction.onsuccess = function (event) {
      console.log('[Transaction] ALL DONE!');
    };

    transaction.onerror = function (event) {
      console.log('[Transaction] NOT DONE!' + transaction);
    };

    let arr;
    const productsStore = transaction.objectStore(Globals.COLLECTION_NAME);
    productsStore.getAll().onsuccess = function (event) {
      arr = JSON.parse(JSON.stringify(event.target.result));
      console.log('the values are: ' + arr);
      resolve(arr);

    };
  }
}

