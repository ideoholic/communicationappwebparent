import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../shared/guard/auth.service';
import { UserRestService } from '../shared/services/userRestService.service';
import { MessageRestService } from '../shared/services/messageRestService.service';
import { IdbService } from '../shared/services/idb.service';

export class MessageElement {
	position: number;
	subject: String;
	description: string;
	group: string;
	time: string;
	date: string;
	userid: string;
	id: string;
}

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
	loginUserData = { username: '', password: '' };
	isPasswordEmpty: boolean = false;
	isUserNameEmpty: boolean = false;
	userID: string;
	private database;

	public obj = {
		name: '',
		contact: '',
		userid: '',
		password: '',
		isAdmin: '',
		id: '',
		group: '',
		username: ''
	};

	constructor(private router: Router, private idb: IdbService, private userService: UserRestService,
		private authService: AuthService, private messageService: MessageRestService) { }



	ngOnInit() {
		this.idb.indexedDataBase(
			(db) => {
				this.database = db;
			}, () => {
				console.error('Failed to create');
			}
		);
	}

	onLogin() {

		if (this.loginUserData.password === null || this.loginUserData.password === undefined
			|| this.loginUserData.password === '') {
			this.isPasswordEmpty = true;
			return;
		}

		if (this.loginUserData.username === null || this.loginUserData.username === undefined
			|| this.loginUserData.username === '') {
			this.isUserNameEmpty = true;
			return;
		}

		this.isUserNameEmpty = false;
		this.isPasswordEmpty = false;

		this.userService.loginUser(this.loginUserData)
			.subscribe(res => {
				let headers = {};
				const keys = res.headers.keys();

				headers = keys.map(key =>
					`${key}: ${res.headers.get(key)}`);

				const token = res.body.token;

				this.authService.setToken(token);
				this.getAdminDetails();
				return;
			},
				err => {
					console.log(err);
					this.router.navigate(['/login']);

					var x = document.getElementById('snackbar');
					x.className = 'show';
					setTimeout(function () { x.className = x.className.replace('show', ''); }, 3000);

				}
			);
	}


	getAdminDetails() {
		this.userService.getDetails()
			.subscribe(res => {
				this.getMessages(res.id,
					() => {
						this.router.navigate(['/dashboard']);
					},
					() => {
						alert('Something went wrong');
						console.error('Something went wrong');
					});
			},
				err => { console.error(err); }
			);

	}

	getMessages(id, resolve, reject) {

		const ELEMENT_DATA: MessageElement[] = [];
		let singleElement: MessageElement;

		this.messageService.getParentMessage(id)
			.subscribe(
				response => {
					var counter = 0;

					response.forEach(element => {
						singleElement = new MessageElement();
						singleElement.position = ++counter;
						singleElement.date = element.date;
						singleElement.time = element.time;
						singleElement.subject = element.subject;
						singleElement.group = element.group;
						singleElement.id = element.id;

						ELEMENT_DATA.push(singleElement);
					});
					this.idb.storeData(this.database, ELEMENT_DATA);
					resolve();
				},
				err => {
					console.error(err);
					reject();
				}
			);

	}
}
