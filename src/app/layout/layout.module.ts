import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {MatSortModule} from '@angular/material/sort';
import { MatDialogModule } from '@angular/material';

import {
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatCardModule,
    MatFormFieldModule,
    MatDatepickerModule, 
    MatNativeDateModule
} from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModuleLoader } from '../material-module';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MessageComponent } from './message/message.component';
import { FormsModule,ReactiveFormsModule, NgModelGroup } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { TopnavComponent } from './components/topnav/topnav.component';
import { DisplaymessageComponent } from './displaymessage/displaymessage.component';


@NgModule({
    imports: [
        MatButtonModule,
        MatCardModule,
        MatDialogModule,
        MaterialModuleLoader,
        MatTableModule,
        MatSortModule,
        MatCheckboxModule,
        CommonModule,
        LayoutRoutingModule,
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatListModule,
        MatCardModule,
        MatTableModule,
        MatDatepickerModule, 
        MatNativeDateModule,
        MatFormFieldModule,
        CdkTableModule,
        TranslateModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [LayoutComponent, TopnavComponent, DisplaymessageComponent,
         DashboardComponent, MessageComponent ],
   
    //    entryComponents: [DialogOverviewExampleDialog,DashboardComponent]     

})
export class LayoutModule { }
