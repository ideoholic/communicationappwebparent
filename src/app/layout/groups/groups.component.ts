import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material';

export interface GroupElement {
    GroupName: string;
    GroupId: number;
    date: string;
    Contact: string;
}

const ELEMENT_DATA: GroupElement[] = [
    { GroupId: 1, GroupName: 'SC-APP', date: '12-01-2019', Contact: '8984562979' },
    { GroupId: 3, GroupName: 'GM-MSG', date: '27-07-2019', Contact: '9745425645' },
    { GroupId: 2, GroupName: 'SM-APP', date: '15-05-2019', Contact: '0497302037' },
    { GroupId: 3, GroupName: 'GM-MSG', date: '17-02-2019', Contact: '9740201701' }
];

@Component({
    selector: 'app-groups',
    templateUrl: './groups.component.html',
    styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements OnInit {
    displayedColumns = ['GroupId', 'GroupName', 'date', 'Contact'];
    dataSource = new MatTableDataSource(ELEMENT_DATA);
    places: Array<any> = [];

    @ViewChild(MatSort, {static: true}) sort: MatSort;

    applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    }

    constructor() {
    }

    ngOnInit() {
       this.dataSource.sort = this.sort;
    }
}
