import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material';
import { UserRestService } from '../../shared/services/userRestService.service';
import { Router } from '@angular/router';
import { AuthService } from '../../shared/guard/auth.service';
import {ActivatedRoute,Params} from '@angular/router';


export class UsersElement {
    username: string;
    position: number;
    contact: string;
    group: string;
    userid: string;
	delete: string;
	view: string;
	id : string; 
}


@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
	
})	
export class UsersComponent implements OnInit {
    displayedColumns = ['position', 'view','username', 'contact', 'userid', 'group','delete'];
    dataSource = new MatTableDataSource([]);
    
    @ViewChild(MatSort, {static: true}) 
	     sort: MatSort;
	     myObj:UsersElement;
		

    applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matche
    }
     
	constructor(private userService: UserRestService,
	private router: Router,
        private authService: AuthService, private route: ActivatedRoute) {}

    ngOnInit() {
		this.displayUsers();
	}
	
		displayUsers(){
            this.userService.getAllUsers()
            .subscribe(response => {
				let ELEMENT_DATA: UsersElement[]= [];

                console.log("Response from server " + response);
				
				let counter=0;
				response.forEach(element=>
				{
					this.myObj=new UsersElement();
					this.myObj.username= element.username;
					this.myObj.position= ++counter;
				    this.myObj.userid= element.userId;
					this.myObj.contact= element.contact;
					this.myObj.group= element.group;
					this.myObj.id = element.id;
				
					ELEMENT_DATA.push(this.myObj);
			
				});
                   this.dataSource = new MatTableDataSource(ELEMENT_DATA);
              },
              error => console.log(error)
            );
        }
		deleteUser(id : string){
		console.log("the user id is "+ id);
	 this.userService.userDelete(id)
	 .subscribe(response=> {
			this.displayUsers(); 
			console.log("id is" +id);
	 },
		error => console.log(error)
            );
	}
		
		onView(id:string){
			console.log("this is id"+id);
            this.userService.getUser(id)
            .subscribe(
                response=>{
					this.router.navigate(['/viewuser', id ]);
					},
                error => console.log(error)
            );

        }
		
		
		
}

