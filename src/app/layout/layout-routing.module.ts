import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MessageComponent } from './message/message.component';
import { DisplaymessageComponent } from './displaymessage/displaymessage.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'dashboard'
            },
            {
                path: 'dashboard',
                component: DashboardComponent
                //loadChildren: './dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'addmessage',
                component: MessageComponent
		//loadChildren: './message/message.module#MessageModule'
            },
            {
               path: 'viewmessage/:id',
               component: DisplaymessageComponent
       //loadChildren: './message/message.module#MessageModule'
            }
		   
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LayoutRoutingModule {}
