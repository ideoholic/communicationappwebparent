import { CommonModule } from '@angular/common';
import { NgModule} from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatCardModule, MatIconModule, MatTableModule } from '@angular/material';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatFormFieldModule } from '@angular/material';
import { MaterialModuleLoader } from '../../material-module';
//import { NewuserComponent } from './newuser.component';

@NgModule({
  imports: [
        CommonModule,
        MatGridListModule,
        MatCardModule,
        MatTableModule,
        MatButtonModule,
        MatIconModule,
        MatFormFieldModule,
        FlexLayoutModule.withConfig({addFlexToParent: false})
  ],
  declarations: []
})
export class NewuserModule { }
