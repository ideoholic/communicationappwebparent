import { Component, OnInit } from '@angular/core';
import { UsersElement } from '../users/users.component';
import {ActivatedRoute,Params} from '@angular/router';
import { UserRestService } from '../../shared/services/userRestService.service';
import { MatTableDataSource } from '@angular/material';
import { GroupElement } from '../groups/groups.component';
import { GroupRestServices } from '../../shared/services/groupRestServices.service';

@Component({
  selector: 'app-newuser',
  templateUrl: './newuser.component.html',
  styleUrls: ['./newuser.component.scss']
})
export class NewuserComponent implements OnInit {
	
	   newuserData ={
	   username :"",
        userId: "",
        password : "",
	    contact : "",
        group: "",
	    id : ""
	}

  constructor(private userService: UserRestService, 
    private route:ActivatedRoute , private groupService: GroupRestServices) { }  
	
  groupArray =[];
  

  ngOnInit() {
	 
  }

  
  userCreated(){
					
	  console.log("the new user is working");
	 // console.log("new user data "  + this.newuserData.username, + this.newuserData.contact ,+this.newuserData.userId );
	this.userService.saveUser(this.newuserData)
	   .subscribe(
		res => {
              console.log("Trying new user");
		},
		err => {
		console.log(err);
		}
		);
	    
}

onSubmit()
{
	 this.groupService.getGroups()
    .subscribe(response=>{
      response.forEach(element => {
        this.groupArray.push(element.group);
		console.log("the group elements "+element.group);
      });
    },
    error => console.log(error)
   );
}
}