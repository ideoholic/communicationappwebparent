import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplaymessageComponent } from './displaymessage.component';

describe('DisplaymessageComponent', () => {
  let component: DisplaymessageComponent;
  let fixture: ComponentFixture<DisplaymessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplaymessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplaymessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
