import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Params} from '@angular/router';
import { MessageRestService } from '../../shared/services/messageRestService.service';
import { MessageElement } from '../dashboard/dashboard.component';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-displaymessage',
  templateUrl: './displaymessage.component.html',
  styleUrls: ['./displaymessage.component.scss']
})
export class DisplaymessageComponent implements OnInit {

  dataSource = {};
  position: number;
    subject: String;
    description: string;
    group: string;
    time: string;
    date: string;
    userid: string;
    delete: string;
    id: string;

  constructor(private messageService: MessageRestService, 
    private route:ActivatedRoute) { }
  
  public MessageId;
  ngOnInit() {

  let id = this.route.snapshot.params['id']
  this.MessageId =id;

  this.fetchUserMessages(this.MessageId);
  }

  fetchUserMessages(MessageId:string){
    var myObj:MessageElement;
    var ELEMENT_DATA: MessageElement[] = [];
    this.messageService.getASingleMessage( this.MessageId)
            .subscribe(
                response=>{
                    console.log("I am here....",response);

                    response.forEach(element => {
                     
                     this.date = element.date;
                     this.time = element.time; 
                     this.subject = element.subject;
                     this.description = element.description;
                     this.group = element.group;
                     
                     });
                },
                error => console.log(error)
              );  
  }

}
