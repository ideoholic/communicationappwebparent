import { Component, OnInit, ViewChild, OnDestroy, OnChanges, Inject } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material';
import { MessageRestService } from '../../shared/services/messageRestService.service';
import { Router } from '@angular/router';
import { AuthService } from '../../shared/guard/auth.service';
import { StorageserviceService } from '../../shared/services/storageservice.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SharingService } from '../../shared/services/sharing.service';
import { IdbService } from '../../shared/services/idb.service';

export class MessageElement {
    position: number;
    subject: String;
    description: string;
    group: string;
    time: string;
    date: string;
    userid: string;
    id: string;
}


@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    displayedColumns = ['position', 'view', 'subject', 'group', 'date', 'time'];
    dataSource = new MatTableDataSource;
    places: Array<any> = [];
    private database;

    public obj = {
        name: '',
        contact: '',
        userid: '',
        password: '',
        isAdmin: '',
        id: '',
        group: '',
        username: ''
    };

    @ViewChild(MatSort, { static: true })
    sort: MatSort;

    applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    }

    constructor(private messageService: MessageRestService,
        private router: Router, public dialog: MatDialog, private idb: IdbService) { }

    ngOnInit() {
        this.idb.indexedDataBase(
            (db) => {
                this.database = db;
                this.displayMessages();
            }, () => {
                console.error('Failed to create');
            }
        );
    }


    displayMessages() {
        const ELEMENT_DATA: MessageElement[] = [];
        let singleElement: MessageElement;
        this.idb.getStoredData(
            this.database,
            (arr) => {
                arr.forEach(element => {

                    singleElement = new MessageElement();
                    singleElement.position = element.position;
                    singleElement.date = element.date;
                    singleElement.time = element.time;
                    singleElement.subject = element.subject;
                    singleElement.group = element.group;
                    singleElement.id = element.id;
                    console.log('....... . .. .' + singleElement.group);
                    ELEMENT_DATA.push(singleElement);
                });
                this.dataSource = new MatTableDataSource(ELEMENT_DATA);
            }
        );
    }

    onView(messageId: string) {
        this.messageService.getASingleMessage(messageId)
            .subscribe(
                response => {
                    this.router.navigate(['/viewmessage', messageId]);
                },
                error => console.log(error)
            );

    }



}
