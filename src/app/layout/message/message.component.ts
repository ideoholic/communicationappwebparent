import { Component, OnInit,Input,ViewChild, OnDestroy } from '@angular/core';
import { GroupRestServices } from '../../shared/services/groupRestServices.service';
import { MessageRestService } from '../../shared/services/messageRestService.service';
import { NgForm } from '@angular/forms' 
import { StorageserviceService } from '../../shared/services/storageservice.service';

export class GroupsElement {
}

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit,OnDestroy {

  @ViewChild('tag',{static:false})
  enteredValues : NgForm;

  isChecked:boolean = false;
  ArrayOfSelectedUsers = [];
  

  Message={
    subject:"",
    description:"",
    group:"",
    timepicker:"",
    datepicker:"",
    users:[]}

  groupObj = new GroupsElement();
  groupArray =[]; 
  disableGroupArray:boolean = false; 

  constructor(private groupService:GroupRestServices,private Storageservice:StorageserviceService,private messageService:MessageRestService) { }

  ngOnInit() {
    this.groupService.getGroups()
    .subscribe(response=>{
      response.forEach(element => {
        this.groupObj= element.groupName;
        this.groupArray.push(this.groupObj);
      });
    },
    error => console.log(error)
   );

   //Get Array of Selescted Users If Any
   this.Storageservice.getSelectedUsers();
  
   if(this.ArrayOfSelectedUsers.length>0){
    this.disableGroupArray=true;
   }else{this.disableGroupArray=false;}
   
  }
  
  Submit(){
    this.Message.subject = this.enteredValues.value.subjectKeyName;
    this.Message.description = this.enteredValues.value.descriptionKeyName;
    if(this.disableGroupArray){
    this.Message.users = this.ArrayOfSelectedUsers;
   //Here placed group any, as group should not be blank 
    this.Message.group = "Any";
    }else{
      this.Message.group = this.enteredValues.value.groupKeyName;
    }
    this.Message.datepicker = this.enteredValues.value.dateKeyName;
    this.Message.timepicker = this.enteredValues.value.timeKeyName;

    console.log(this.Message.subject)
   //Store Message
       this.messageService.setMessage(this.Message)
    .subscribe(response=>{   
      //Clear the form field
       this.enteredValues.resetForm();
      
       //Add toast
       var x = document.getElementById("snackbar");
       x.className = "show";
       setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    },
    error => console.log(error)
   );
  }

  ngOnDestroy(){
    this.Storageservice.setArrayEmpty();
  }

 
}


