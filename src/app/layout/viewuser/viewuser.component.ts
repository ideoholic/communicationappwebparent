import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Params} from '@angular/router';
import { UserRestService } from '../../shared/services/userRestService.service';
import { UsersElement } from '../users/users.component';
import { MatTableDataSource } from '@angular/material';


@Component({
  selector: 'app-viewuser',
  templateUrl: './viewuser.component.html',
  styleUrls: ['./viewuser.component.scss']
})
export class ViewuserComponent implements OnInit {
	
  dataSource = {};
	    username : '';
        userID: string;
         password : string;
	    contact : string;
        group: string;
	    id : string;

  constructor(private userService: UserRestService, 
    private route:ActivatedRoute) { }
	public userid;
	

  ngOnInit() {
	  let id = this.route.snapshot.params['id']
  this.userid =id;
    this.FunCall(this.userid);

  }
  
  FunCall(userid:string){
    var myObj:UsersElement;
    var ELEMENT_DATA: UsersElement[] = [];
	
    this.userService.getUser( this.userid)
            .subscribe(
                response=> {
                    console.log("In viewuser....",response);
	

					this.username=response.username;         
                     this.group = response.group;
                     this.contact = response.contact;
					 this.userID= response.userId;
					 this.password = response.password;
                },
                error => console.log(error)
              );  
  }
  }

 